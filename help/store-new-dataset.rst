Store a new dataset 
===================

`Storing a new dataset <https://sysimm.org/interclone/datasets/new>`_ is easy as long as the files conform to the subset of AIRR standards required by InterClone. Please see the requirements and example datasets in order to understand what valid input looks like. If you do not know how to prepare AIRR-formatted files, please take a look at `preparing AIRR files <./prepare-AIRR.html>`_.

One or more AIRR-formatted files should be combined as a zip file (`see here how to do this <https://www.wikihow.com/Make-a-Zip-File>`_). To begin the process of storing a new dataset, please click the ”Store” menu item.

Give your dataset a name. This name can be anything, but we recommend following a convention, such as “<author>-<date>” so that it's easy to browse later.

Next select the appropriate “Receptor Type” and “Chain Type”. Single-cell sequencing data should be stored as “paired”, while bulk data for a given chain should be uploaded separately.  Please note that InterClone does not explicitly distinguish between species, but most of the data is human.

Next, provide tags for your data. Tags are used to filter dataset for use in search or cluster jobs. Typically, the species, or descriptions of the donors (healthy, etc.) should be provided.

When you are all ready, browse for the zip file, and click “Store Dataset”. Please be patient, as each sequence will be annotated (CDR regions defined, encoded and stored on our local filesystem.) Depending on the load on our server and the size of your data, this can take from minutes to hours.
