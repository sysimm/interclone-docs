Cluster datasets
================

`Clustering <https://sysimm.org/interclone/cluster>`_ involves selecting one or more datasets, then clicking “Cluster”. You will be directed to a waiting page while the job completes (which should take a few minutes or less). The results page should load automatically with a URL you can bookmark. The results consist of a table of clusters, sorted by decreasing size.You can download a summary of the clusters as a TSV or XLS file. You can also download an expanded table, which consists of the original AIRR file with additional columns containing the clusters.


To follow a real world use case with inputs and results, please see the `tutorial <../tutorial/clustering-tcr-datasets.html>`_.
