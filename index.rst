InterClone
==========

The InterClone webserver and associated database provides easily accessible tools for storing, searching and clustering adaptive immune receptor repertoire (AIRR) sequence datasets. InterClone was designed to allow users to control the visibility of their own data. To this end data consists of “public” datasets, which are visible to all, and “private” datasets, which are visible only to the user who stored the data. To create a private dataset, you need to `create an account <https://sysimm.org/interclone/register/>`_, which is free to do. The source code of the backend pipeline is available on `Gitlab <https://gitlab.com/sysimm/interclone>`_.


.. toctree::
   :maxdepth: 2
   :caption: Help

   help/store-new-dataset
   help/search-datasets
   help/cluster-datasets
   help/prepare-AIRR


The following is a walk-through for the three use cases described in the paper:

.. toctree::
   :maxdepth: 2
   :caption: Tutorial

   tutorial/storing-new-dataset
   tutorial/searching-bcr-datasets
   tutorial/clustering-tcr-datasets
  

All described functions are accessible without the need for a user account. However, additional features are available to those who register, like storing private datasets and reusing previous search queries. Registration is free and simple.

Report an Issue
---------------

This service is still under development. If you encounter any problems, please don't hesitate to `contact us <https://sysimm.org/contact-pages/contact-us-about-interclone>`_ about them.
