======================
Searching BCR datasets
======================

We will investigate the distribution of infection enhancing antibodies in both disease and healthy donors. For this, we will need the full length amino acid sequences of the enhancing antibodies, which can be obtained from `Cov-AbDab <http://opig.stats.ox.ac.uk/webapps/covabdab/>`_. For example, the antibody 8D2 has the following sequence:

EVQLVESGGGLVQPGGSLRLSCAASGFTFSSYWMSWVRQAPGKGLEWVANINQDGSEKYYVDSVKGRFTISRDNAKNSLYLQVNSLRAEDTAVYYCARDWDYDILTGSWFGAFDIWGQGTTVTVSS

The results for the COVID19 search can be accessed `here <https://sysimm.ifrec.osaka-u.ac.jp/interclone/search/125>`_. The results for the healthy search can be accessed `here <https://sysimm.ifrec.osaka-u.ac.jp/interclone/search/149>`_.

Searching COVID19 data
----------------------

On the InterClone web server, select the `Search function <https://sysimm.ifrec.osaka-u.ac.jp/interclone/search>`_ and insert the above sequence into the “Query sequence” field. Enter a name for the query (e.g. “8D2”) and, optionally, some tags (e.g. “COVID19”). Then choose the appropriate sequence identity cutoff values, i.e. 80/80/70 for CDRs 1, 2 and 3, respectively, and 80 for the coverage. Then, search for the “Kim-2021” dataset in the table of target datasets. You can filter the results by name, type and tags. Choose “Use as target” in the last column and the name of the dataset will appear in the “Selected targets” section, which will also indicate the total number of sequences that are about to be searched. The input form should look like this:

.. image:: ../images/bcr-search-form-covid.png

Click the “Search” button and wait for the result, which should appear after a few minutes. The progress will be indicated on the results page and will automatically reload so that once the search has finished, you should see this result:

.. image:: ../images/bcr-search-results-covid.png

This table shows a summary of the search hits: the matched query and template sequences as well as their similarity scores, separately for each CDR. By clicking on “Download expanded results”, you can access additional metadata from the original inputs that might be useful for further analysis, e.g. “clone_count”.

Searching healthy data
----------------------

We will now repeat the above search for healthy data. If you are logged in, you can select the previously used query on the search page. Otherwise, you will have to re-enter the sequence. Again, select the appropriate thresholds (80, 80, 70 and 80). This time, filter the target datasets table for the following three datasets and select them as search targets:

    * Gidoni-2019
    * Ghraichy-2020
    * Meng-2017

The input form should look like this, assuming you are reusing the previous query:

.. image:: ../images/bcr-search-form-healthy.png

Press “Search” and wait for the results, which should look like this:

.. image:: ../images/bcr-search-results-healthy.png

This time, there are fewer search hits than for the COVID19 data but as mentioned above, it’s important to consider clonal expansion by checking the clone_count column in the extended results.
The above steps can be repeated for all 11 known enhancing antibodies. The separate results can then be aggregated.

